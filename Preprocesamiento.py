import pandas as pd 

# Cargar los datos
file_path = input('Ingrese la ruta al archivo con los datos de CASEN 2022 en formato csv: ')
# file_path = 'datos/data-case.csv'
data = pd.read_csv(file_path)

# Definir columnas relevantes para hogares incluyendo las nuevas variables seleccionadas
columnas_hogares = [
    'id_vivienda', 'area', 'region', 'tot_per_h', 'p1', 'v1', 'v9', 'v13', 'v20', 'v34a', 'v34c', 'n_nucleos', 'v21', 'v22',
    'y0101h', 'y0301h', 'y0302h', 'y0303h', 'yfamh', 'y2001h', 'y2301h', 'y2604h', 'y2701h', 'y2803h', 'ymon02h',
    'y280202h', 'y2804h', 'y280101h', 'yre1h', 'yre2h', 'ytroh', 'yah2h', 'yruth', 'yac2h', 'y2401h', 'yaimh', 'y0304h',
    'ytoth', 'ypc', 'pobreza', 'li', 'lp', 'nae', 'yae'
]

# Agrupar por ID de vivienda y tomar el primer registro como representante del hogar
df_hogares = data[columnas_hogares].groupby('id_vivienda').first().reset_index()

# Verificar el DataFrame resultante
# print(df_hogares.head())

# data.drop(data.index, inplace=True)

# codigo_region = 5
# df_hogares_filtrado = df_hogares[df_hogares['region'] == codigo_region]
# df_hogares_filtrado.to_csv('df_hogares_quinta.csv', index=False)

df_hogares.to_csv('datos/hogares.csv', index=False)